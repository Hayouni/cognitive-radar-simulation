%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Milestone one: Development of Matlab model of phase array radar    %%%
%%%  Date : 06/11/2018                                                  %%%
%%%  Scenario : Phased Array Radar tracking 3 moving targets            %%%
%%%  In This work we have taken advantage of the phased array toolbox   %%%
%%%  and the examples presented by matlab.                              %%%
%%%  Matlab version used : Matlab R2018a                                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%this code will be split into bloack:
%1) Phased array radar design
%2) Active Scanning to aquire information about the target
%2) Using the target extracted information we predict the next position of
%the target .

clc;
clear;
close all;
%% ^^^^^^^^^^^^^^ block (1) phased array radar design ^^^^^^^^^^^^^^^^^^^^

%%%%%%%%%%%%%%%%%%%%%<< Design Specifications >>%%%%%%%%%%%%%%%%%%%%%%%%%%%
%the design aims to detect targets at a max distance of 6km with at least 1
%square meter rcs.the probability of detection and probability of false
%alarm are set to be 0.9 and 10-5 respectively
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
jamming= false;
pd = 0.9;            % Probability of detection is set to 0.9 fixed
pfa = 1e-5;          % Probability of false alarm is set to 10-5 fixed
max_range = 150000;    % Maximum  range
range_res = 100;      % Required range resolution
tgt_rcs = 1;         % Required target radar cross section
int_pulsenum = 1;   % Number of pulses to integrate
targetNature=[true];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%<< waveform >>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the waveform will be later disgned adaptively when implementing a
% cognitive radar. for now we design the waveform a rectangularwaveorm. the
% pulse bandwidth is defined by the desired range resolution, the prf is
% defined by the maximum range
prop_speed = physconst('LightSpeed');   % Propagation speed
pulse_bw = prop_speed/(2*range_res);    % Pulse bandwidth for a resolution of 50m
pulse_width = 1/pulse_bw;               % Pulse width
prf = prop_speed/(2*max_range);         % Pulse repetition frequency(5000km max range)
prfinit=prf;
fs = 2*pulse_bw;                        % Sampling rate is twice as the bandwidth
waveform = phased.RectangularWaveform('PulseWidth',1/pulse_bw,'PRF',prf,'SampleRate',fs);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%<< Phased Array Components >>%%%%%%%%%%%%%%%%
% we will design 4 components : receiver , transmitter ,  radiator and
% collector
%  Receiver
% we disgn the receiver to have 20 db gain and 0db noise figure
receiver = phased.ReceiverPreamp('Gain',20,'NoiseFigure',0,'SampleRate',fs,'EnableInputPort',true);


% transmitter
% we design the transmitter to have 20db gain. the peak power is set to a
% fixed value of 10KW
tx_gain = 20;
fc = 45e8; % fc is chosen such that lambda =0.1(0.05 lelement spacing) fc=c/lambda

peak_power =10000; %peak power is set to fixed value of 10kw
transmitter = phased.Transmitter('Gain',tx_gain,'PeakPower',peak_power,'InUseOutputPort',true);


% radiator and collector
% the Radiator and Collector allow the antenna to radiate and collect the
% signal in the form of electromagnatic wave. therefore we design first the
% antenne

antenna = phased.IsotropicAntennaElement(...
    'FrequencyRange',[5e8 20e10]);

sensormotion = phased.Platform(...
    'InitialPosition',[0; 0; 23],...
    'Velocity',[0; 0; 0]); % we assume the ship using this antenna is
%stationary and the antenna is 23 meters above see level
% the radiator and collector operate on the frequency fc
radiator = phased.Radiator(...
    'Sensor',antenna,...
    'OperatingFrequency',fc);

collector = phased.Collector(...
    'Sensor',antenna,...
    'OperatingFrequency',fc);
collector1 = phased.Collector(...
    'Sensor',antenna,...
    'OperatingFrequency',fc);



v = radiator.PropagationSpeed;            % Wave propagation speed (m/s)
lambda = v/fc;                         % Wavelength (m), lambda was designed to be 0.1 (0.05 element spacing)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%<< define N by N array of antenna >>%%%%%%%%%%%%%%%%%%%%%
%planar array antenna , 2m by 2m squre array , with 5cm spacining between
%antenna element
ura = phased.URA('Element',antenna,'Size',[29 33],'ElementSpacing',[lambda/2,lambda/2]);
% Configure the antenna elements such that they only transmit forward
ura.Element.BackBaffled = true;

%  ********* Visualize the response pattern.  ************
% figure1 presents the 3d response pattern of the array radiation
figure(1);
pattern(ura,fc,'PropagationSpeed',physconst('LightSpeed'),...
    'Type','powerdb');
% figure 2 and 3 represents an azimuth cut of the propagated signal ( 0deg
% elevation), main lobe is directed to 0 deg azimuth
figure(2);
pattern(ura,fc,[-180:180],0,...
    'Type','directivity',...
    'PropagationSpeed',physconst('LightSpeed'));
figure(3);
fmt = 'rectangular';
cutAngle = 0;
pattern(ura, fc, -180:180, cutAngle, 'PropagationSpeed', physconst('LightSpeed'), 'Type', ...
    'directivity', 'CoordinateSystem', fmt );
%figure 4 presents a 3d directivity pattern
figure(4);
fmt = 'uv';
pattern(ura, fc, 'PropagationSpeed', physconst('LightSpeed'), 'Type','directivity', ...
    'CoordinateSystem', fmt);


%associate the radiator and collector with the array
radiator.Sensor = ura;
collector.Sensor = ura;
% We need to set the WeightsInputPort property to true to enable it to
% accept transmit beamforming weights
radiator.WeightsInputPort = true;
% Calculate the array gain ,
arraygain = phased.ArrayGain('SensorArray',ura,'PropagationSpeed',v);
ag = arraygain(fc,[0;0]);

% Use the radar equation to calculate the peak power
% snr_min = albersheim(pd, pfa);%using albersheim method we calculate
% %the signal to noise ratio such that pd and pfa are 10-5 and 0.9
% snr_min=snr_min/int_pulsenum;
% %recalculate peak_power
% peak_power = radareqpow(lambda,max_range,snr_min,waveform.PulseWidth,...
%     'RCS',tgt_rcs,'Gain',transmitter.Gain + ag);
% % Set the peak power of the transmitter
%  transmitter.PeakPower = peak_power;

%%%%%%% next we design the scan schedule such that our antenna covers the
%%%%%%% the angles [-45, 45] deg azimuth and [0 ,60] elevation
%Scan squedule
initialAz = 45; endAz = -45;
initialEl = -1; endEl = 1;
volumnAz = initialAz - endAz;
% Calculate 3-dB beamwidth in deg
beamwidth = rad2deg(sqrt(4*pi/db2pow(ag)));
scanstep =-round(beamwidth/2,0) ;%the scan step can't be greater than beamwidth, for the sake of having
%grahps we use a step of beamwidth/3
scangrid = initialAz:scanstep:endAz;
scanelev=initialEl:1:endEl;
numscans = length(scangrid);
pulsenum = int_pulsenum*numscans;
pulsnuminit=pulsenum;
conventional_revisit_time=pulsnuminit/prfinit;

%%%%%%%%% now we have split our scan area into length(scangrid) X length(scanelv) matrix


%%%%%% << Createa a jamming signal >>%%%%%%%%%%%%%%%%%%%%%%%%
rng default
jammer = phased.BarrageJammer('ERP',1,...
    'SamplesPerFrame',200);
jamsig = jammer();
jammerpos=[4000;0;0];
jammervel=[10;2;10];
jammermotion = phased.Platform('InitialPosition',jammerpos,'Velocity',jammervel);
[jamrng,jamang] = rangeangle(jammermotion.InitialPosition,sensormotion.InitialPosition);

% Define propagation channel for jammming signal
jammerchannel = phased.FreeSpace('TwoWayPropagation',false,...
    'SampleRate',fs,'OperatingFrequency', fc);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%<<       %TARGET Creation       >>%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%target description:
tgtpos = [7000; 5000; 20]; % targets position vector
% tgtpos = [110050; 50000; 20]; % targets position vector
tgtvel = [60; 50; 0]; %targets velocity vector
tgtsp=-norm(tgtvel);
tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
tgtrcs = 10; %targets cross section vector
%target creation
target = phased.RadarTarget('MeanRCS',tgtrcs,'OperatingFrequency',fc);

% Calculate the range, angle, and speed of the targets
[tgtrng,tgtang] = rangeangle(tgtmotion.InitialPosition,sensormotion.InitialPosition);
% Number of targets
numtargets = length(target.MeanRCS);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% creating the friendly unitis database %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
friendpos = [110000; 5000; 20]; % targets position vector
friendvel = [100; 80; 0]; %targets velocity vector
friendsp=-norm(friendvel);
friendmotion = phased.Platform('InitialPosition',friendpos,'Velocity',friendvel);
tgtrcs = 10; %targets cross section vector
%target creation
target = phased.RadarTarget('MeanRCS',tgtrcs,'OperatingFrequency',fc);

% Calculate the range, angle, and speed of the targets
[friendrng,friendang] = rangeangle(tgtmotion.InitialPosition,sensormotion.InitialPosition);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create the steering vector for transmit beamforming
steeringvec = phased.SteeringVector('SensorArray',ura,'PropagationSpeed',v);
% Create the receiving beamformer
beamformer = phased.PhaseShiftBeamformer('SensorArray',ura,...
    'OperatingFrequency',fc,'PropagationSpeed',v,'DirectionSource','Input port');%a beamformere is used to point to the steering direction to obtain the combined signal
% Define propagation channel for each target
channel = phased.FreeSpace(...
    'SampleRate',fs,...
    'TwoWayPropagation',true,...
    'OperatingFrequency',fc);
fast_time_grid = unigrid(0, 1/fs, 1/prf, '[)');

% Pre-allocate array
rxpulses = zeros(numel(fast_time_grid),pulsenum);

prevpeak=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Revisit time before cognition
revisitTime = pulsenum/prf;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  ^^^^^^^^^^^^^^ (2) Radar active scanning  ^^^^^^^^^^^^^^^^^^^^^^
%%%%%%%%%%%%% << Steering the radar from -45deg to 45 deg Azumith and from
% 0 to 60deg elev >>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5%

for n=1:length(scanelev)
    jamsig = jammer();
    % Update sensor and target positions
    [sensorpos,sensorvel] = sensormotion(1/prf);
    [tgtpos,tgtvel] = tgtmotion(1/prf);
    [friendpos,friendvel] = friendmotion(1/prf);
    % Calculate the target angles as seen by the sensor
    [tgtrng,tgtang] = rangeangle(tgtpos,sensorpos);
    
    
    for m = 1:pulsenum

        % Calculate steering vector for current scan angle
        scanid = floor((m-1)/int_pulsenum) + 1;
        sv = steeringvec(fc,[scangrid(scanid);scanelev(n)]);
        w = conj(sv);
        % Form transmit beam for this scan angle and simulate propagation
        pulse = waveform();
        [txsig,txstatus] = transmitter(pulse);
        txsig = radiator(txsig,tgtang,w);
        txsig = channel(txsig,sensorpos,tgtpos,sensorvel,tgtvel);
        % Reflect pulse off of targets
        tgtsig = target(txsig);

        %%%%%<<<<<<< in case jamming is active we include the jamming signal >>%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Beamform the target returns received at the sensor
        rxsig = collector(tgtsig,tgtang);

        rxsig = receiver(rxsig,~(txstatus>0));
        rxpulses(:,m) = beamformer(rxsig,[scangrid(scanid);scanelev(n)]);
        TotalPulses=rxpulses;

        %%%%% << Process the received signal using matched filters>>%%%%%%
        % Matched filtering
        %we use a matched filter to maximize the signal to noise ration (snr)
        matchingcoeff = getMatchedFilter(waveform);
        matchedfilter = phased.MatchedFilter(...
            'Coefficients',matchingcoeff,...
            'GainOutputPort',true);
        [mf_pulses, mfgain] = matchedfilter(TotalPulses);
        mf_pulses = reshape(mf_pulses,[],int_pulsenum,numscans);

        matchingdelay = size(matchingcoeff,1)-1;
        sz_mfpulses = size(mf_pulses);
        mf_pulses = [mf_pulses(matchingdelay+1:end) zeros(1,matchingdelay)];
        mf_pulses = reshape(mf_pulses,sz_mfpulses);

        % Pulse integration
        int_pulses = pulsint(mf_pulses,'coherent');
        int_pulses = squeeze(int_pulses);

        % Visualize the target in a radar scene
        figure(5);
        Elev_pulses=int_pulses;
        r = v*fast_time_grid/2;
        XX = r'*cosd(scangrid); YY = r'*sind(scangrid);
        clf;
        pcolor(XX,YY,pow2db(abs(Elev_pulses).^2));
        axis equal tight
        shading interp
        axis off
%         text(-800,0,'NPAR');
        text((max(r)+10)*cosd(initialAz),(max(r)+10)*sind(initialAz),...
            [num2str(initialAz) '^o']);
        text((max(r)+10)*cosd(endAz),(max(r)+10)*sind(endAz),...
            [num2str(endAz) '^o']);
        text((max(r)+10)*cosd(0),(max(r)+10)*sind(0),[num2str(0) '^o']);
        colorbar;
        title(['Radar scan for elevation layer ' num2str(n)])




        range_gates = v*fast_time_grid/2;
        tvg = phased.TimeVaryingGain(...
            'RangeLoss',2*fspl(range_gates,lambda),...
            'ReferenceLoss',2*fspl(max(range_gates),lambda));
        tvg_pulses = tvg(mf_pulses);

        % Pulse integration
        int_pulses = pulsint(tvg_pulses,'coherent');
        int_pulses = squeeze(int_pulses);

        % Calculate the detection threshold

        % sample rate is twice the noise bandwidth in the design system
        noise_bw = receiver.SampleRate/2;
        npower = noisepow(noise_bw,receiver.NoiseFigure,receiver.ReferenceTemperature);
        threshold = npower * db2pow(npwgnthresh(pfa,int_pulsenum,'coherent'));
        % Increase the threshold by the matched filter processing gain
        threshold = threshold*db2pow(mfgain);

        N = 51;
        %%plot the threshold along with the proceessed received signal in one
        %%graph
        figure(6);
        surf(XX(N:end,:),YY(N:end,:),pow2db(abs(int_pulses(N:end,:)).^2));
        hold on;
        mesh(XX(N:end,:),YY(N:end,:),...
            pow2db(threshold*ones(size(XX(N:end,:)))),'EdgeColor','flat','FaceAlpha',0.1);
        view(0,56);
        axis off



        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%finding where the peaks are of the signals that excced the
        %%threshhold 
        [~,peakInd] = findpeaks(abs(int_pulses(:)),'MinPeakHeight',sqrt(threshold));
        [maxval,Max] = max(int_pulses(peakInd));
         peakInd=peakInd(Max);
        [rngInd,angInd] = ind2sub(size(int_pulses),peakInd);
        peak=int_pulses(peakInd);
        % Estimated range
        if ~isempty(angInd)
            est_angle = scangrid(angInd);
            est_range = range_gates(rngInd);
            tgtnum=[1:length(est_range)];


            if ~isempty(est_range)
                if (abs(peak)>prevpeak)
                    elev=scanelev(n); % this will give us an approximation of the elevation 
                    prevpeak=abs(peak);
                end
            end

            if(length(tgtnum)==length(tgtrcs))
                figure(7);
                %%% plot the resal range and the estimated range
                plot(tgtnum,est_range,'*','MarkerEdgeColor','r');

                hold on;
                plot(tgtnum,tgtrng,'O','MarkerEdgeColor','b');
                legend({'Real Range','Estimated Range'})

                title('Targets Estimated Ranges Vs Real Ranges');
                axis([0 5 1000 20000]);
                xlabel('Target number') % x-axis label
                ylabel('Target Range (m)') % y-axis label





                [TotalPulses, mfgain] = matchedfilter(TotalPulses);


                % compensate the matched filter delay
                matchingdelay = size(matchingcoeff,1)-1;
                TotalPulses = buffer(TotalPulses(matchingdelay+1:end),size(TotalPulses,1));

                % apply time varying gain to compensate the range dependent loss
                prop_speed = radiator.PropagationSpeed;
                range_gates = prop_speed*fast_time_grid/2;
                lambda = prop_speed/fc;

                tvg = phased.TimeVaryingGain(...
                    'RangeLoss',2*fspl(range_gates,lambda),...
                    'ReferenceLoss',2*fspl(prop_speed/(prf*2),lambda));

                TotalPulses = tvg(TotalPulses);
                Elev_pulses=TotalPulses;
                % Target speed estimation (doppler)
                for m = numtargets:-1:1
                    [p, f] = periodogram(Elev_pulses(rngInd(m),:),[],256,prf,'power','centered');
                    speed_vec = dop2speed(f,lambda)/2;

                    spectrum_data = p/max(p);
                    % plot of the doppler spectrum of the tracked targets
                    figure(9);
                    subplot(2,2,m)
                    plot(pow2db(spectrum_data));
                    title(['Doppler spectrum of target ' num2str(m)])
                    xlabel('Frequency KHz') % x-axis label
                    ylabel('Power (dB)') % y-axis label

                    [~,dop_detect1] = findpeaks(pow2db(spectrum_data),'MinPeakHeight',-5);
                    sp(m) = (speed_vec(dop_detect1(1))); % Estimated Doppler speed
                end

                %plot of the Range-speed response
                figure(8);
                plot(est_range,sp,'gs',...
                    'LineWidth',2,...
                    'MarkerSize',10,...
                    'MarkerEdgeColor','r',...
                    'MarkerFaceColor',[0.5,0.5,0.5]);
                hold on;
                plot(tgtrng,tgtsp,'gs',...
                    'LineWidth',2,...
                    'MarkerSize',10,...
                    'MarkerEdgeColor','b',...
                    'MarkerFaceColor',[0.5,0.5,0.5]);
                legend({'Estimated Range-speed response','Real Range-speed response'});
                title('Targets Range-Speed Response');
                xlabel('Range(m)') % x-axis label
                ylabel('Speed(m/s)') % y-axis label
                axis([ 1000 5000 -210 210]);

                figure(10);
                rangedopresp = phased.RangeDopplerResponse('SampleRate',fs, ...
                    'PropagationSpeed',prop_speed,'DopplerFFTLengthSource','Property', ...
                    'DopplerFFTLength',pulsenum,'DopplerOutput','Speed', ...
                    'OperatingFrequency',fc);
                [rngdopresp,rnggrid,dopgrid] = rangedopresp(Elev_pulses,matchingcoeff);
                imagesc(dopgrid,rnggrid,10*log10(abs(rngdopresp)))
                xlabel('Speed (m/s)')
                ylabel('Range (m)')
                title('Targets Range-Speed Response');
                axis xy
            end
        end
    end
    Elev_pulses=zeros(numel(fast_time_grid),numscans);
    TotalPulses=zeros(numel(fast_time_grid),numscans);
    rxpulses = zeros(numel(fast_time_grid),pulsenum);
    int_pulses = zeros(numel(fast_time_grid),pulsenum);
    % rxpulses(1,:)=zeros(200,1);
end
%% (3) Target Estimator
% use the extracted target information from the echo ( positio and
% estimated speed)
Px=est_range*cosd(elev)*cosd(est_angle);
Py=est_range*cosd(elev)*sind(est_angle);
Pz=23+est_range*sind(elev);

Vx(1)=sp*cosd(elev)*cosd(est_angle);
Vxavg(1)=sp*cosd(elev)*cosd(est_angle);
Vy(1)=sp*cosd(elev)*sind(est_angle);
Vyavg(1)=sp*cosd(elev)*sind(est_angle);
Vz(1)=sp*sind(elev)*cosd(est_angle);
Vzavg(1)=sp*sind(elev)*cosd(est_angle);
Vav(1)=0;
d=0.1;% sampling time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% < defining kalman matrices >%%%%%%%%%%%%%%%%%
% we define the matrix A such that S(n)=A*S(n-1)+w
A=[1 0 0 d 0 0;
    0 1 0 0 d 0;
    0 0 1 0 0 d;
    0 0 0 1 0 0;
    0 0 0 0 1 0;
    0 0 0 0 0 1]; 
% we define the matrix A such that S(n)=A*S(n-1)+w
AminusI=[0 0 0 d 0 0;
    0 0 0 0 d 0;
    0 0 0 0 0 d;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0]; 
% we define the matrix P and Q such that P=A*P*A'+Q

P(:,:,1)=[0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0]; %inital covariance of prediction .we assume that the initial state is known

Q=[0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0.001 0 0;
    0 0 0 0 0.001 0;
    0 0 0 0 0 0.001];% Covarience matrix of process noise

% we define the Covarience matrix of measurment noise that would be used to compute the kalman gain
M=[0.001 0 0;
    0 0.001 0;
    0 0 0.001]; % Covarience matrix of measurment noise

H=[1,0,0,0,0,0;
    0,1,0,0,0,0;
    0,0,1,0,0,0];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%< Target initial information >%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X(:,1)=[Px;Py;Pz;Vx(1);Vy(1);Vz(1)]; % Actual initial conditions
Z(:,1)=[Px;Py;Pz];% initial observation
Xestimation(:,1)=[Px;Py;Pz;Vx(1);Vy(1);Vz(1)];%Assumed initial conditions

est_rng=est_range;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% Pre-allocate array  to speed simulation
N=400;

H = zeros(3,6,N+1);
K = zeros(6,3,N+1);
 prevestimatedangle=tgtang(1);
 prevrealangle=tgtang(1);
 
    prevestimaterange=tgtrng(1);
    prevrealrange=tgtrng(1);
    preintpulse=1;
    prevrange=tgtrng(1);
    prevsnr=0;
    prevsnrmin=0;
% indicator function. Used for unwrapping of tan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\
%%%%%%%%%%%%%%%%%%% <  Kalman filter magic  >%%%%%%%%%%%%%%%%%%%%%%
 range_res = 100;      % Required range resolution
    % Number of pulses to integrate
    
% The maximum range that can be achieved for different number of pulses
% integrated coherently
figure(21);


[Pd1,SNR1] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',1);
[Pd2,SNR2] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',2);
[Pd3,SNR3] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',3);
[Pd4,SNR4] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',4);
[Pd5,SNR5] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',5);

k=1;
while Pd1(k)<0.9
    k=k+1;
end
snr_min1=SNR1(k);
k=1;
while Pd2(k)<0.9
    k=k+1;
end
snr_min2=SNR2(k);
k=1;
while Pd3(k)<0.9
    k=k+1;
end
snr_min3=SNR3(k);
k=1;
while Pd4(k)<0.9
    k=k+1;
end
snr_min4=SNR4(k);
k=1;
while Pd5(k)<0.9
    k=k+1;
end
snr_min5=SNR5(k);

 pow=[1:1200];
 for i =1:length(pow)

 maxrng1(i) = radareqrng(lambda,snr_min1,pow(i),waveform.PulseWidth,'rcs',tgt_rcs,'gain',transmitter.Gain + ag);
  maxrng2(i) = radareqrng(lambda,snr_min2,pow(i),waveform.PulseWidth,'rcs',tgt_rcs,'gain',transmitter.Gain + ag);
   maxrng3(i) = radareqrng(lambda,snr_min3,pow(i),waveform.PulseWidth,'rcs',tgt_rcs,'gain',transmitter.Gain + ag);
    maxrng4(i) = radareqrng(lambda,snr_min4,pow(i),waveform.PulseWidth,'rcs',tgt_rcs,'gain',transmitter.Gain + ag);
     maxrng5(i) = radareqrng(lambda,snr_min5,pow(i),waveform.PulseWidth,'rcs',tgt_rcs,'gain',transmitter.Gain + ag);
 end
 plot(pow,maxrng1);
 hold on 
 plot(pow,maxrng2);
 hold on 
 plot(pow,maxrng3);
 hold on 
 plot(pow,maxrng4);
 hold on 
 plot(pow,maxrng5);
 xlabel('Transmit power(W)')
 ylabel('Maximum Range')
 title('Maximum range for different number of pulses integrated')
 legend('N=1', 'N=2','N=3', 'N=4','N=5');
 
 %initialisations 
  scan_angle=est_angle; 
  measangle(1)=est_angle; % apply moving average fiklter to measured angle
  mavgangle(1)=measangle(1);
  measrange(1)=est_range;
  mavgrange(1)=est_range;
  smoothangle(1)=est_angle;
   smoothestangle(1)=est_angle;
  prevcognitiverevisitTime=conventional_revisit_time;
  sPx(1)=Py/Px;
   Xchangeindex=1;
    Ychangeindex=1;
     Zchangeindex=1;
 
for n=1:N

     if n==100
     tgtvel = [60; 0; 0];
     tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
     end
     if n==200
     tgtvel = [60; -50; 0];
     tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
     end
     if n==300
     tgtvel = [30; 50; 0];
     tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
     end
%      if n==140
%      tgtvel = [-20; -80; 0];
%      tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
%      end
%      if n==160
%      tgtvel = [-80; -80; 0];
%      tgtmotion = phased.Platform('InitialPosition',tgtpos,'Velocity',tgtvel);
%      end

    rngInd=[];
    angInd=[];
    prevpeak=0;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Waveform optimization in light of the target estimations , the
    %%% following waveform parameters will be changed adaptively :
    %PRF , PW ,fs , N number of pulses ,duty cucle ,radar schedule

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    max_range =  100*(fix(est_range/100)+5)+500;    % Waveform optimization , Maximum  range
    pulse_bw = prop_speed/(2*range_res);    % Pulse bandwidth for a resolution of 50m
    pulse_width = 1/pulse_bw;               % Pulse width
    prevprf=prf;
    prf = prop_speed/(2*max_range);         % Waveform optimisation Pulse repetition frequency
    fs = 2*pulse_bw;                        % Sampling rate is twice as the bandwidth
    waveform = phased.RectangularWaveform('PulseWidth',1/pulse_bw,'PRF',prf,'SampleRate',fs);
    pulse = waveform();
    receiver = phased.ReceiverPreamp('Gain',30,'NoiseFigure',0,'SampleRate',fs,'EnableInputPort',true);

    max_power=1000;
    release( transmitter) ;
    transmitter = phased.Transmitter('Gain',tx_gain,'PeakPower',peak_power,'InUseOutputPort',true);


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Deciding the number of pulses to integrate based on the max transmit
   % power and the target estimated range
   
    SNR=radareqsnr(lambda,est_rng,max_power,waveform.PulseWidth,'RCS',tgt_rcs,'Gain',transmitter.Gain + ag);
    snr_min=SNR+1;
    int_pulsenum = 0;
    while snr_min>SNR
         int_pulsenum=int_pulsenum+1;
        [Matrix_Pd,Matrix_SNR] = rocpfa(pfa,'SignalType','Nonfluctuatingcoherent','NumPulses',int_pulsenum);

            k=1;
            while Matrix_Pd(k)<0.9
                k=k+1;
            end

         snr_min=Matrix_SNR(k);
   
    end
    
    
     figure(22)
    xlabel('Frames')
    ylabel('SNR')
    title('SNR Track')
%     axis([120000 150000 0 10]);
    line([n,n+1],[prevsnr,SNR],'Color','b','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on;
      line([n,n+1],[prevsnrmin,snr_min],'LineStyle','-.','Color','r','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on;
    legend('Track SNR','SNR required for detection')
    drawnow
    
    
    figure(20)
    xlabel('Target range (m)')
    ylabel('Number of pulses')
    title('Waveform design:Adaptively changing the number of pulses to integrate')
%     axis([120000 150000 0 10]);
    line([prevrange,est_rng],[preintpulse,int_pulsenum],'Color','r','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on;
    drawnow
    
    preintpulse=int_pulsenum;
    prevrange=est_rng;
    prevsnr=SNR;
    prevsnrmin=snr_min;
    
    figure(18)
    line([n,n+1],[tgtang(2),tgtang(2)],'LineStyle','--','Color','g','LineWidth',2)
    hold on
    line([n,n+1],[elev,elev],'LineStyle','-.','Color','r','LineWidth',2)
    hold on
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  adaptively changing the transmit power to provide just enough power
    %  for the estimated range
    peak_power = radareqpow(lambda,max_range,snr_min,waveform.PulseWidth,'RCS',tgt_rcs,'Gain',transmitter.Gain + ag);
    transmitter.PeakPower = peak_power;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Optimizing the radar scan schedule
    initialAz = scan_angle+0.5; endAz = scan_angle-0.5;
    initialEl = elev;
    scangrid = initialAz:-0.1:endAz;
    scanelev=elev;
    numscans = length(scangrid);
    pulsenum = int_pulsenum*numscans;
    fast_time_grid = unigrid(0, 1/fs, 1/prf, '[)');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Elev_pulses=zeros(numel(fast_time_grid),pulsenum);
    TotalPulses=zeros(numel(fast_time_grid),pulsenum);
    rxpulses = zeros(numel(fast_time_grid),pulsenum);

  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%  Radar mesurement %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % Update sensor and target positions
     
   
    [sensorpos,sensorvel] = sensormotion(0.1);
    previoustgtpos=tgtpos;
    [tgtpos,tgtvel] = tgtmotion(0.1);
    [friendpos,friendvel] = friendmotion(0.1);
    % Calculate the target angles as seen by the sensor
    [friendrng,friendang] = rangeangle(friendpos,sensorpos);
    [tgtrng,tgtang] = rangeangle(tgtpos,sensorpos);
    % Calculate steering vector for current scan angle
    rxpulses = zeros(numel(fast_time_grid),pulsenum);
    for e=1:length(scanelev)

    for m = 1:pulsenum


        scanid = floor((m-1)/int_pulsenum) + 1;
        sv = steeringvec(fc,[scangrid(scanid);scanelev(e)]);
        w = conj(sv);
        % Form transmit beam for this scan angle and simulate propagation

        [txsig,txstatus] = transmitter(pulse);
        txsig = radiator(txsig,tgtang,w);
        txsig = channel(txsig,sensorpos,tgtpos,sensorvel,tgtvel);
        % Reflect pulse off of targets
        tgtsig = target(txsig);

        %%%%%<<<<<<< in case jamming is active we include the jamming signal >>%
        % Propagate the jamming signal to the array
        if(jamming)
            jamsig = jammerchannel(jamsig,jammerpos,sensorpos,jammervel,sensorvel);
            % Collect the jamming signal
            jamsig = collector1(jamsig,jamang);

        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Beamform the target returns received at the sensor
        rxsig = collector(tgtsig,tgtang);
        rxsig = receiver(rxsig,~(txstatus>0));
        rxpulses(:,m) = beamformer(rxsig,[scangrid(scanid);elev]);









        TotalPulses=rxpulses;

        %%%%% << Process the received signal using matched filters>>%%%%%%
        % Matched filtering
        %we use a matched filter to maximize the signal to noise ration (snr)
        matchingcoeff = getMatchedFilter(waveform);
        matchedfilter = phased.MatchedFilter(...
            'Coefficients',matchingcoeff,...
            'GainOutputPort',true);
        [mf_pulses, mfgain] = matchedfilter(TotalPulses);
        mf_pulses = reshape(mf_pulses,[],int_pulsenum,numscans);

        matchingdelay = size(matchingcoeff,1)-1;
        sz_mfpulses = size(mf_pulses);
        mf_pulses = [mf_pulses(matchingdelay+1:end) zeros(1,matchingdelay)];
        mf_pulses = reshape(mf_pulses,sz_mfpulses);

        % Pulse integration
        int_pulses = pulsint(mf_pulses,'coherent');
        int_pulses = squeeze(int_pulses);

        % Visualize the target in a radar scene
        figure(5);
        Elev_pulses=int_pulses;
        r = v*fast_time_grid/2;
        XX = r'*cosd(scangrid); YY = r'*sind(scangrid);
        clf;
        pcolor(XX,YY,pow2db(abs(Elev_pulses).^2));
        axis equal tight
        shading interp
        axis off
%         text(-800,0,'NPAR');
        text((max(r)+10)*cosd(initialAz),(max(r)+10)*sind(initialAz),[num2str(initialAz) '^o']);
        text((max(r)+10)*cosd(endAz),(max(r)+10)*sind(endAz),[num2str(endAz) '^o']);
        text((max(r)+10)*cosd(0),(max(r)+10)*sind(0),[num2str(0) '^o']);
        %add target inforamtion to the graph
       
        text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-18),['Tracking time (s): ' num2str(0.1*n)],'Color','b','LineWidth',2);
        text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-15),['Target range (m): ' num2str(est_range)],'Color','b','LineWidth',2);
        text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-12),['Target Azimuth angle (deg): ' num2str(est_angle)],'Color','b','LineWidth',2);
        text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-9),['Target elevation angle (deg): ' num2str(elev)],'Color','b','LineWidth',2);
        text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-6),['Target coordinates (m): [' num2str(Px) ',' num2str(Py) ',' num2str(Pz) ']'],'Color','b','LineWidth',2);
        if targetNature
            text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-3),'Target Nature: ENEMY','LineStyle','-.','Color','r','LineWidth',2);
        else
            text((max(r)+10)*cosd(endAz)/8,(max(r)+10)*sind(est_angle-3),'Target Nature: FRIENDLY ','LineStyle','--','Color','g','LineWidth',2);
        end
        title(['Tacking target at range in meters= ' num2str(est_range)])




        range_gates = v*fast_time_grid/2;
        tvg = phased.TimeVaryingGain(...
            'RangeLoss',2*fspl(range_gates,lambda),...
            'ReferenceLoss',2*fspl(max(range_gates),lambda));
        tvg_pulses = tvg(mf_pulses);

        % Pulse integration
        int_pulses = pulsint(tvg_pulses,'coherent');
        int_pulses = squeeze(int_pulses);

        % Calculate the detection threshold

        % sample rate is twice the noise bandwidth in the design system
        noise_bw = receiver.SampleRate/2;
        npower = noisepow(noise_bw,...
            receiver.NoiseFigure,receiver.ReferenceTemperature);
        threshold = npower * db2pow(npwgnthresh(pfa,int_pulsenum,'coherent'));
        % Increase the threshold by the matched filter processing gain
        threshold = threshold*db2pow(mfgain);

        N = 51;
        %%plot the threshold along with the proceessed received signal in one
        %%graph
        %uncomment if graph 6 is desired ( plotting unnecessary graphs
        %slows the simulation
%         figure(6);
%         surf(XX(N:end,:),YY(N:end,:),...
%             pow2db(abs(int_pulses(N:end,:)).^2));
%         hold on;
%         mesh(XX(N:end,:),YY(N:end,:),...
%             pow2db(threshold*ones(size(XX(N:end,:)))),'EdgeColor','flat','FaceAlpha',0.1);
%         view(0,56);
%         axis off
%         

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%finding where the peaks are
        [~,peakInd] = findpeaks(abs(int_pulses(:)),'MinPeakHeight',sqrt(threshold));
        [maxval,Max] = max(int_pulses(peakInd));
        peakInd=peakInd(Max);
        [rngInd,angInd] = ind2sub(size(int_pulses),peakInd);
        peak=int_pulses(peakInd);
        % Estimated range
        if ~isempty(angInd)
            est_angle = scangrid(angInd);
            measangle(n+1)=est_angle;
            est_range = range_gates(rngInd);
            measrange(n+1)=est_range(1);
       %deciding the nature of target(true=enemy, false=friendly)
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       if abs(est_range-friendrng)>100
            targetNature=true;
       else
            targetNature=false;
       end
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            tgtnum=[1:length(est_range)];
            % estimating the elevation angle
            if(int_pulses(peakInd)>prevpeak)
               elev= scanelev(e);
               prevpeak=peak;
            end

            if(length(tgtnum)==length(tgtrcs))
                figure(7);
                %%% plot the resal range and the estimated range
                plot(tgtnum,est_range,'*','MarkerEdgeColor','r');

                hold on;
                plot(tgtnum,tgtrng,'O','MarkerEdgeColor','b');
                legend({'Real Range','Estimated Range'})

                title('Targets Estimated Ranges Vs Real Ranges');
           
                xlabel('Target number') % x-axis label
                ylabel('Target Range (m)') % y-axis label



                [TotalPulses, mfgain] = matchedfilter(TotalPulses);
                % compensate the matched filter delay
                matchingdelay = size(matchingcoeff,1)-1;
                TotalPulses = buffer(TotalPulses(matchingdelay+1:end),size(TotalPulses,1));
                % apply time varying gain to compensate the range dependent loss
                prop_speed = radiator.PropagationSpeed;
                range_gates = prop_speed*fast_time_grid/2;
                lambda = prop_speed/fc;

                tvg = phased.TimeVaryingGain(...
                    'RangeLoss',2*fspl(range_gates,lambda),...
                    'ReferenceLoss',2*fspl(prop_speed/(prf*2),lambda));
                Elev_pulses=TotalPulses;
                TotalPulses = tvg(TotalPulses);
                
                % Target speed estimation (doppler)
                for m = numtargets:-1:1
                    [p, f] = periodogram(mf_pulses(rngInd(m),:),[],256,prf,'power','centered');
                    speed_vec = dop2speed(f,lambda)/2;
                    max_speed = dop2speed(prf/2,lambda)/2;

                    spectrum_data = p/max(p);
%                     plot of the doppler spectrum of the tracked targets
%                     uncomment to plot graph9
                    figure(9);
                    subplot(2,2,m)
                    plot(pow2db(spectrum_data));
                    title(['Doppler spectrum of target ' num2str(m)])
                    xlabel('Frequency KHz') % x-axis label
                    ylabel('Power (dB)') % y-axis label

                    [~,dop_detect1] = findpeaks(pow2db(spectrum_data),'MinPeakHeight',-5);
                    if ~isempty(dop_detect1)
                    sp(m) = (speed_vec(dop_detect1(1))); % Estimated Doppler speed
                    end
                end

                %plot of the Range-speed response
                % uncomment if graph 8 is wanted , plotting more graphs
                % slows down the simulation
%                 figure(8);
%                 plot(est_range,sp,'gs',...
%                     'LineWidth',2,...
%                     'MarkerSize',10,...
%                     'MarkerEdgeColor','r',...
%                     'MarkerFaceColor',[0.5,0.5,0.5]);
%                 hold on;
%                 plot(tgtrng,tgtsp,'gs',...
%                     'LineWidth',2,...
%                     'MarkerSize',10,...
%                     'MarkerEdgeColor','b',...
%                     'MarkerFaceColor',[0.5,0.5,0.5]);
%                 legend({'Estimated Range-speed response','Real Range-speed response'});
%                 title('Targets Range-Speed Response');
%                 xlabel('Range(m)') % x-axis label
%                 ylabel('Speed(m/s)') % y-axis label
%                 axis([ 1000 5000 -210 210]);
    % uncomment to plot graph 10
%                 figure(10);
%                 rangedopresp = phased.RangeDopplerResponse('SampleRate',fs, ...
%                     'PropagationSpeed',prop_speed,'DopplerFFTLengthSource','Property', ...
%                     'DopplerFFTLength',pulsenum,'DopplerOutput','Speed', ...
%                     'OperatingFrequency',fc);
%                 [rngdopresp,rnggrid,dopgrid] = rangedopresp(Elev_pulses,matchingcoeff);
%                 imagesc(dopgrid,rnggrid,10*log10(abs(rngdopresp)))
%                 xlabel('Speed (m/s)')
%                 ylabel('Range (m)')
%                 title('Targets Range-Speed Response');
%                 axis xy
            end
        end

    end
  
    end
    % rxpulses(1,:)=zeros(200,1);


    %% (3) Target Estimator
    %it consists of 5 steps:
    %Step 1:Prediction of current state
    %Step 2:Prediction of state covariance
    %Step 3:Kalman Gain Matix
    %Step 4:Corrected estimates of s(n)
    %Step 5:Posterior state covariance
    
    % use the extracted target information from the echo ( position and
    % estimated speed)
%     mvg=0;
%     for count = 1:n+1
%    mvg=mvg+measangle(count)*count/(2*sum(1:n+1));
%     end

    mavgangle(n+1)=sum(measangle(4*floor(n/4)+1:n+1))/length(measangle(4*floor(n/4)+1:n+1));
    mavgangle(n+1)=sum(mavgangle(2*floor(n/2)+1:n+1))/length(mavgangle(2*floor(n/2)+1:n+1));
    mavgrange(n+1)=sum(measrange(4*floor(n/4)+1:n+1))/length(measrange(4*floor(n/4)+1:n+1));
    mavgrange(n+1)=(mavgrange(n+1)+mavgrange(n))/2;
    Px=measrange(n+1)*cosd(elev)*cosd(mavgangle(n+1));
    Py=measrange(n+1)*cosd(elev)*sind(mavgangle(n+1));
    Pz=23+est_range(1)*sind(elev);
    
 
 

    % Target estimator state mesurements
    X(:,n+1)=[0.5*(Px(1)+X(1,n)),0.5*(Py(1)+X(2,n)),0.5*(Pz(1)+X(3,n)),Vx(1),Vy(1),Vz(1)];
    Z(:,n+1)=[0.5*(Px(1)+X(1,n)),0.5*(Py(1)+X(2,n)),0.5*(Pz(1)+X(3,n))];

       sPx(n+1)=(Xestimation(2,n))/(Xestimation(1,n));
   
 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




    % Step1 and Step2
    [Xestimation(:,n+1),P(:,:,n+1)]=predict(A,Xestimation(:,n),P(:,:,n),Q); %prediction of next state
    % Computing the matrix H such that z=H*s+w
    H(:,:,n+1)=Hmatrix();
    % Step 3 
    K(:,:,n+1)=KalmanGain(H(:,:,n+1),P(:,:,n+1),M); %Kalman Gain
    %Step 4
    Error=Correction(Z(:,n+1),Xestimation(:,n+1)); %prediction error
    Xestimation(:,n+1)=Xestimation(:,n+1)+ K(:,:,n+1)*Error; %Correct estimate
    % Step 5
    P(:,:,n+1)=(eye(6)-K(:,:,n+1)*H(:,:,n+1))*P(:,:,n+1);% % covarience of estimation error

%     est_angle=(asind(Xestimation(2)/(est_range(1))*cosd(elev))+est_angle)/2;
% estimation of the target azimuth angle wich will be used to steer the
% beam 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Revisit time %%%%%%%%%%%
    
    cognitiverevisitTime = pulsenum/prf;

    figure(16) 
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('Revisit time (ms)')
    title('Target Revisit Time for Cognitive radars')
    hold on
    line([n,n+1],[1000*prevcognitiverevisitTime,1000*cognitiverevisitTime],'Color','b','LineWidth',2);
    hold on
    line([n,n+1],[1000*conventional_revisit_time,1000*conventional_revisit_time],'LineStyle','-.','Color','r','LineWidth',2);
    legend('CR target revisit time', 'Conventional radar target revisit time');
  

    prevcognitiverevisitTime=cognitiverevisitTime;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(13)
 
    line([n,n+1],[measangle(n),measangle(n+1)],'Color','b','LineWidth',2);
    hold on
    line([n,n+1],[mavgangle(n),mavgangle(n+1)],'Color','m','LineWidth',2);
    hold on
    est_angle=(Xestimation(2,n+1)/abs(Xestimation(2,n+1)))*acosd(Xestimation(1,n+1)/(est_range(1)*cosd(elev)));
     smoothangle(n+1)=(smoothangle(n)+est_angle)/2;
%      smoothestangle(n+1)=sum(smoothangle)/length(smoothangle);
%     smoothestangle(n+1)=sum(smoothangle((20*floor(n/20)+1):n+1))/length(smoothangle((20*floor(n/20)+1):n+1));
   
     
    scan_angle=est_angle;
% plot the stimated angle versus the real angle 
     figure(13)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('Azimuth angle in deg')
    title('Azimuth angle ( estimated vs real)')
   
    line([n,n+1],[prevestimatedangle,est_angle],'LineStyle','-.','Color','r','LineWidth',2);
    hold on
    line([n,n+1],[prevrealangle,tgtang(1)],'LineStyle','--','Color','g','LineWidth',2);
%     hold on
%     line([n,n+1],[smoothangle(n),smoothangle(n+1)],'Color','b','LineWidth',2);
    hold on
%      line([n,n+1],[smoothestangle(n),smoothestangle(n+1)],'Color','y','LineWidth',2);
%     hold on

    legend('measured', 'filtered mesured','estimated', 'real');
    % plot the stimated range versus the real range 
     figure(14)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('Target Range (m)')
    title('Target range')
    hold on
    est_rng=sqrt(Xestimation(1,n)^2+Xestimation(2,n)^2+Xestimation(3,n)^2);
    line([n,n+1],[prevestimaterange,est_rng],'Color','r','LineWidth',2);
    hold on
    line([n,n+1],[prevrealrange,tgtrng(1)],'LineStyle','--','Color','g','LineWidth',2);
    hold on
     line([n,n+1],[measrange(n),measrange(n+1)],'LineStyle','-.','Color','m','LineWidth',2);
    hold on
    line([n,n+1],[mavgrange(n),mavgrange(n+1)],'Color','b','LineWidth',2);
    hold on
    legend('estimated', 'real','measured','filtered measurment');
    
     figure(15)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('PRF')
    title('Radar Pulse Repetition Frequency')
    hold on
    line([n,n+1],[prevprf,prf],'Color','r','LineWidth',2);
    hold on

    
    Vx(n+1)=(Xestimation(4,n+1)+Xestimation(4,n))/2;
     Vxavg(n+1)=sum(Vx(Xchangeindex+1:n))/length(Vx(Xchangeindex+1:n));
%     Vxavg(n+1)=sum(Vx(100*floor(n/100)+1:n+1))/length(Vx(100*floor(n/100)+1:n+1));
    %     Vxavg(n+1)=sum(Vx)/length(Vx);
    
    Vy(n+1)=(Xestimation(5,n+1)+Xestimation(5,n))/2;
%     Vyavg(n+1)=sum(Vy(100*floor(n/100)+1:n+1))/length(Vy(100*floor(n/100)+1:n+1));
     Vyavg(n+1)=sum(Vy(Ychangeindex+1:n))/length(Vy(Ychangeindex+1:n));

    Vz(n+1)=(Xestimation(6,n+1)+Xestimation(6,n))/2;
    Vzavg(n+1)=sum(Vz)/length(Vz);
    
    if (est_rng<50000)
        Vthreshold=30;
    elseif(est_rng>50000 && est_rng<100000)
        Vthreshold=90;
    elseif(est_rng>100000)
        Vthreshold=100;
    end
    
    if (abs(Vxavg(n+1)-Vx(n+1))>Vthreshold)
       Xchangeindex=n; 
    end
    if (abs(Vyavg(n+1)-Vy(n+1))>Vthreshold)
       Ychangeindex=n; 
    end
    
    figure(25)
    subplot(2,1,1)
    line([n,n+1],[abs(Vxavg(n)-Vx(n)),abs(Vxavg(n+1)-Vx(n+1))],'Color','b','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on
    title('Target Velocity deviation');
    xlabel('Frames')
    ylabel('Vx (m/s)')
    subplot(2,1,2)
    line([n,n+1],[abs(Vyavg(n)-Vy(n)),abs(Vyavg(n+1)-Vy(n+1))],'Color','b','LineWidth',2)
    hold on
    title('Target Velocity deviation');
    xlabel('Frames')
    ylabel('Vy (m/s)')
    
% plot est speed
 figure(23)
    subplot(4,1,1)
    line([n,n+1],[Vx(n),Vx(n+1)],'LineStyle','-.','Color','r','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on
     line([n,n+1],[tgtvel(1),tgtvel(1)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
     line([n,n+1],[Vxavg(n),Vxavg(n+1)],'Color','b','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    title('Target Velocity estimation');
    legend('Estimated','Real','Filterd Estimated velocity')
     xlabel('Frames')
    ylabel('Vx (m/s)')
    drawnow
    figure(23)
    subplot(4,1,2)
    line([n,n+1],[Vy(n),Vy(n+1)],'LineStyle','-.','Color','r','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    line([n,n+1],[tgtvel(2),tgtvel(2)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    line([n,n+1],[Vyavg(n),Vyavg(n+1)],'Color','b','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    legend('Estimated','Real','Filterd Estimated velocity')
    xlabel('Frames')
    ylabel('Vy (m/s)')
    figure(23)
    subplot(4,1,3)
    line([n,n+1],[Vz(n),Vz(n+1)],'LineStyle','-.','Color','r','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
     line([n,n+1],[tgtvel(3),tgtvel(3)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
    line([n,n+1],[Vzavg(n),Vzavg(n+1)],'Color','b','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    legend('Estimated','Real','Filterd Estimated velocity')
     xlabel('Frames')
    ylabel('Vz (m/s)')
    figure(23)
     subplot(4,1,4)
    line([n,n+1],[norm([Vx(n),Vy(n),Vz(n)]),norm([Vx(n+1),Vy(n+1),Vz(n+1)])],'LineStyle','-.','Color','r','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
     line([n,n+1],[norm(tgtvel),norm(tgtvel)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
    line([n,n+1],[norm([Vxavg(n),Vyavg(n),Vzavg(n)]),norm([Vxavg(n+1),Vyavg(n+1),Vzavg(n+1)])],'Color','b','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    legend('Estimated','Real','Filterd Estimated velocity')
     xlabel('Frames')
    ylabel('Radial velocity (m/s)')
    

    
    
    
    prevestimatedangle=est_angle;
    prevrealangle=tgtang(1);
    prevestimaterange=est_rng;
    prevrealrange=tgtrng(1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generating the plots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(17)
    xlabel('X(m)')
    ylabel('Y(m)')
    title('The Cartesian target track with adaptive radar waveform parameters')
    line([Xestimation(1,n),Xestimation(1,n+1)],[Xestimation(2,n),Xestimation(2,n+1)],'LineStyle','-.','Color','r','LineWidth',2,'Marker','*')%plotting estimated trajectory for y coordinate
    hold on
    line([X(1,n),X(1,n+1)],[X(2,n),X(2,n+1)],'LineStyle','--','Color','b','LineWidth',2,'Marker','o')  % plot of the process that we try to observe in z coordinate
    hold on 
    line([previoustgtpos(1),tgtpos(1)],[previoustgtpos(2),tgtpos(2)],'Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
   
    legend('predicted position','Measured position','target track');
        
        
    figure(24)
    xlabel('Frames')
    ylabel('prediciton error')
    title('prediction error ')
    line([n,n+1],[Xchangeindex,Xchangeindex])
    hold on
     line([n,n+1],[Ychangeindex,Ychangeindex],'LineStyle','-.','Color','r','LineWidth',2)
    hold on
 
  
    
    
    figure(11)
    subplot(3,2,1)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('X')
    title('X possition')
    hold on
    figure(11)
    subplot(3,2,3)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('Y')
    title('Y possition')
    hold on
    figure(11)

    subplot(3,2,5)
    xlabel('Sample(Time=0.1*Sample)')
    ylabel('Z')
    title('Z possition')
    hold on
    figure(11)
    subplot(3,2,2)
    xlabel('Sample(Time=0.1*Sample)')
    title('Minimum MSE')
    hold on
    figure(11)
    subplot(3,2,4)
    xlabel('Sample(Time=0.1*Sample)')
    title('Minimum MSE')
    hold on
    figure(11)
    subplot(3,2,6)
    xlabel('Sample(Time=0.1*Sample)')
    title('Minimum MSE')
    hold on
    figure(11)




    subplot(3,2,2)
    line([n,n+1],[(X(1,n)-Xestimation(1,n))^2,(X(1,n+1)-Xestimation(1,n+1))^2],'Color','r','LineWidth',2) % ploting MSE
    hold on
    drawnow
    legend('Square erro in X direction')
    figure(11)
    subplot(3,2,4)
    line([n,n+1],[(X(2,n)-Xestimation(2,n))^2,(X(2,n+1)-Xestimation(2,n+1))^2],'Color','r','LineWidth',2) % ploting MSE
    hold on
    drawnow
    legend('Square erro in Y direction')
    figure(11)
    subplot(3,2,6)
    line([n,n+1],[(X(3,n)-Xestimation(3,n))^2,(X(3,n+1)-Xestimation(3,n+1))^2],'Color','r','LineWidth',2) % ploting MSE
    hold on
    drawnow
    legend('Square erro in Z direction')

    figure(11)
    subplot(3,2,1)
    line([n,n+1],[Xestimation(1,n),Xestimation(1,n+1)],'Color','r','LineWidth',2) %plotting estimated trajectory for x coordinate
    hold on
    drawnow
    figure(11)
    subplot(3,2,1)
    line([n,n+1],[X(1,n),X(1,n+1)],'LineStyle','-.','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    figure(11)
    subplot(3,2,1)
    line([n,n+1],[previoustgtpos(1),tgtpos(1)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
 
    
    legend('Estimated X Position','Mesured X Position','Real position','Location','Best')

    figure(11)
    subplot(3,2,3)
    line([n,n+1],[Xestimation(2,n),Xestimation(2,n+1)],'Color','r','LineWidth',2)%plotting estimated trajectory for y coordinate
    hold on
    drawnow
    figure(11)
    subplot(3,2,3)
    line([n,n+1],[X(2,n),X(2,n+1)],'LineStyle','-.','LineWidth',2)  % plot of the process that we try to observe in y coordinate
    hold on
    figure(11)
    subplot(3,2,3)
    line([n,n+1],[previoustgtpos(2),tgtpos(2)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
  
  
    legend('Estimated Y Position','Mesured Y Position','Real position','Location','Best')


    figure(11)
    subplot(3,2,5)
    line([n,n+1],[Xestimation(3,n),Xestimation(3,n+1)],'Color','r','LineWidth',2) %plotting estimated trajectory for z coordinate
    hold on
    drawnow
    figure(11)
    subplot(3,2,5)
    line([n,n+1],[X(3,n),X(3,n+1)],'LineStyle','-.','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
    figure(11)
    subplot(3,2,5)
    line([n,n+1],[previoustgtpos(3),tgtpos(3)],'LineStyle','--','Color','g','LineWidth',2)  % plot of the process that we try to observe in z coordinate
    hold on
    drawnow
   
    legend('Estimated Z Position','Mesured Z Position','Real position')

    figure(12)
    plot3(0,0,0)
    title('3-D trajectory ')
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
    hold on


    line([Xestimation(1,n) Xestimation(1,n+1)],[Xestimation(2,n) Xestimation(2,n+1)],[Xestimation(3,n) Xestimation(3,n+1)],'LineStyle','-.','Color','r','LineWidth',2)% 3-d plot of estimated trajectory
    hold on


    line([X(1,n) X(1,n+1)],[X(2,n) X(2,n+1)],[X(3,n) X(3,n+1)])% 3-d plot of actual trajectory
    hold on

    

    


end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%this function calculates the error of prediction
function   Error=Correction(Z,Xestimation)

Zer=[Xestimation(1);Xestimation(2);Xestimation(3)];
Error=Z-Zer;% Estimation Error

end

%this function computes the Hmatrix matrix H
function  H=Hmatrix()

H=[1,0,0,0,0,0;
    0,1,0,0,0,0;
    0,0,1,0,0,0];

end

%this function computes the kalman gain
function   K=KalmanGain(H,P,M)

K=P*H'*(M+H*P*H')^(-1);

end

%This function does step1 and step 2
function   [Xestimation,P]=predict(A,Xestimation,P,Q)

Xestimation=A*Xestimation;% prediction
P=A*P*A'+Q;% covariance of prediction

end


